module gitlab.com/eemj/log

go 1.13

require (
	github.com/natefinch/lumberjack v2.0.0+incompatible
	go.uber.org/zap v1.13.0
)
