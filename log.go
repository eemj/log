package log

import (
	"os"
	"path/filepath"
	"runtime"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"github.com/natefinch/lumberjack"
)

type Logger struct {
	AppName string
	*zap.SugaredLogger
}

var log Logger

func getFileLogger() (core zapcore.Core) {
	config := zap.NewProductionEncoderConfig()
	config.EncodeTime = zapcore.ISO8601TimeEncoder
	config.EncodeLevel = zapcore.CapitalLevelEncoder
	config.EncodeDuration = zapcore.NanosDurationEncoder

	encoder := zapcore.NewJSONEncoder(config)

	wd, _ := os.Getwd()

	core = zapcore.NewCore(
		encoder,
		zapcore.AddSync(&lumberjack.Logger{
			Filename:   filepath.Join(wd, (log.AppName + ".log")),
			MaxSize:    50,
			MaxBackups: 10,
			MaxAge:     28,
		}),
		zapcore.DebugLevel,
	)

	return
}

func getConsoleLogger() (core zapcore.Core) {
	config := zap.NewProductionEncoderConfig()
	config.EncodeTime = zapcore.ISO8601TimeEncoder

	if runtime.GOOS != "windows" {
		config.EncodeLevel = zapcore.LowercaseColorLevelEncoder
	} else {
		config.EncodeLevel = zapcore.LowercaseLevelEncoder
	}

	config.EncodeDuration = zapcore.NanosDurationEncoder

	encoder := zapcore.NewConsoleEncoder(config)

	core = zapcore.NewCore(
		encoder,
		zapcore.AddSync(os.Stdout),
		zapcore.DebugLevel,
	)

	return
}

func Setup(name string) (err error) {
	core := zapcore.NewTee(
		getFileLogger(),
		getConsoleLogger(),
	)

	prod := zap.New(core)

	log = Logger{
		SugaredLogger: prod.Sugar(),
		AppName:       name,
	}

	return
}
